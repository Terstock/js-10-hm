// Теоретичні питання
// 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

// створення - document.createElement("a"),
//   innerHtml,
//   insertAdjacentHTML(),
//   document.createTextNode(),
//   cloneNode();
// додавання - beforebegin, afterbegin, beforeend, afterend,

// 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
//1. створюємо змінну і отримуємо посилання на елемент через метод - отримаємо або його id або класс,
//2. видаляємо його через метод node.remove()

// 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
//a.append, a.prepend, a.before, a.after,

// Практичні завдання
//  1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

// let a = document.createElement("a");
// a.textContent = "Learn More";
// a.setAttribute("href", "#");

// let fut = document.querySelector("#footers");
// fut.append(a);

// console.log(fut);
//  2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
//  Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

let select = document.createElement("select");
select.id = "rating";

let main = document.querySelector("#mainus");
main.prepend(select);

for (let i = 1; i < 5; i++) {
  let option = document.createElement("option");
  option.setAttribute("value", `${i}`);
  if (i === 1) {
    option.textContent = `${i} Star`;
  } else {
    option.textContent = `${i} Stars`;
  }

  select.append(option);
}

console.log(select);
